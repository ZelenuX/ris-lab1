package org.example.ris1;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.LogManager;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Main {

    public static final QName USER = QName.valueOf("user");
    public static final QName CHANGESET = QName.valueOf("changeset");
    public static final QName K = QName.valueOf("k");

    public static void main(String[] args) {
        try {
            var logger = LogManager.getLogger("Lab1");
            var startTime = System.currentTimeMillis();

            var cmdLine = readArgs(args);
            var length = Long.parseLong(Optional.ofNullable(cmdLine.getOptionValue('l')).orElse(String.valueOf(Long.MAX_VALUE)));
            var inPath = cmdLine.getOptionValue('x');
            logger.info("Reading from: {}", inPath);

            try (var in = new BZip2CompressorInputStream(new BufferedInputStream(new FileInputStream(inPath)))) {
                XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
                XMLEventReader reader = xmlInputFactory.createXMLEventReader(in);
                var usersChangeSets = new HashMap<String, Map<String, Integer>>();
                var keysNodes = new HashMap<String, Integer>();
                boolean insideNode = false;
                while (reader.hasNext() && in.getBytesRead() < length) {
                    var nextEvent = reader.nextEvent();
                    if (nextEvent.isStartElement()) {
                        var startElement = nextEvent.asStartElement();
                        if ("node".equals(startElement.getName().getLocalPart())) {
                            insideNode = true;
                            var attrUser = startElement.getAttributeByName(USER);
                            var attrChangeSet = startElement.getAttributeByName(CHANGESET);
                            if (attrUser == null || attrChangeSet == null) {
                                continue;
                            }
                            var user = attrUser.getValue();
                            var changeSet = attrChangeSet.getValue();
                            usersChangeSets.computeIfAbsent(user, key -> new HashMap<>())
                                    .merge(changeSet, 1, Integer::sum);
                        }
                        if ("tag".equals(startElement.getName().getLocalPart()) && insideNode) {
                            var attrKey = startElement.getAttributeByName(K);
                            if (attrKey == null) {
                                continue;
                            }
                            keysNodes.merge(attrKey.getValue(), 1, Integer::sum);
                        }
                    }
                    if (nextEvent.isEndElement()) {
                        var endElement = nextEvent.asEndElement();
                        if ("node".equals(endElement.getName().getLocalPart())) {
                            insideNode = false;
                        }
                    }
                }

                usersChangeSets.entrySet().stream()
                        .sorted((a, b) -> {
                            var aChangeSets = a.getValue().size();
                            var bChangeSets = b.getValue().size();
                            return -Integer.compare(aChangeSets, bChangeSets);
                        })
                        .forEach(userChangeSets -> {
                            var user = userChangeSets.getKey();
                            var changeSets = userChangeSets.getValue();
                            System.out.println(user + "   "
                                    + changeSets.size() + "   "
                                    + changeSets.values().stream().reduce(Integer::sum).orElse(0));
                        });

                System.out.println("\n------------------------------------------------\n");
                keysNodes.entrySet().stream()
                        .sorted((a, b) -> -Integer.compare(a.getValue(), b.getValue()))
                        .forEach(entry -> System.out.println(entry.getKey() + "   " + entry.getValue()));
            }
            logger.info("Done in " + (System.currentTimeMillis() - startTime) + "ms");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static CommandLine readArgs(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("x", "xml", true, "Xml input");
        options.addOption("l", "length", true, "Read N bytes");
        options.addOption("o", "out", true, "Xml output");
        DefaultParser parser = new DefaultParser();
        return parser.parse(options, args);
    }
}
